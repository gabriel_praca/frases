
// Imports
import React from 'react';
import { Alert, Text, AppRegistry, View, Image, TouchableOpacity } from 'react-native';

const Styles = {
  viewStyle: {
    flex: 1,
    backgroundColor: '#fff',

    //seção 7- aula 86
    //alinha os componentes internos verticalmente
    justifyContent: 'center',
    //alinha os componentes internos horizontalmente
    alignItems: 'center',
    //flexDirection 'column' um componente fica em cima do outro, 'row' fica um do lado do outro
    flexDirection: 'column',
  },
  logo: {

  },
  buttonStyle: {
    marginTop: 50,
    backgroundColor: '#fff',
    padding: 10,
    borderColor: '#538530',
    borderWidth: 2,
    borderRadius: 10,

    // Só funciona em iphone
    //  shadowColor: 'black',
    //  shadowOffset: { width: 0, height: 5 },
    //  shadowOpacity: 1,
    //
  },
    buttonTextStyle: {
      color: '#538530',
      fontSize: 16,
      fontWeight: 'bold',
      alignSelf: 'center',
    }
};

// Styles
const { viewStyle, logo, buttonStyle, buttonTextStyle } = Styles;

export default class App extends React.Component {
  render() {
    return (
      <View style={viewStyle}>

        <Image style={logo} source={require('./imgs/logo.png')} />
        <TouchableOpacity 
          onPress={newMessageButton}
          style={buttonStyle}>

          <Text style={buttonTextStyle}>Nova Mensagem</Text>

        </TouchableOpacity>

      </View>
    );
  }
}

const newMessageButton = () => {

  //Messages
  var messages = Array();
  messages[0] = 'O sucesso nasce do querer, da determinação e persistência em se chegar a um objetivo. Mesmo não atingindo o alvo, quem busca e vence obstáculos, no mínimo fará coisas admiráveis';
  messages[1] = 'Determinação, coragem e auto-confiança são fatores decisivos para o sucesso. Se estamos possuídos por uma inabalável determinação, conseguiremos superá-los. Independentemente das circunstâncias, devemos ser sempre humildes, recatados e despidos de orgulho';
  messages[2] = 'Agir, eis a inteligência verdadeira. Serei o que quiser. Mas tenho que querer o que for. O êxito está em ter êxito, e não em ter condições de êxito. Condições de palácio tem qualquer terra larga, mas onde estará o palácio se não o fizerem ali?';
  messages[3] = 'Lute. Acredite. Conquiste. Perca. Deseje. Espere. Alcance. Invada. Caia. Seja tudo o quiser ser, mas acima de tudo, seja você sempre';
  messages[4] = 'Só existe um êxito: a capacidade de levar a vida que se quer';

  var randomNum = Math.floor(Math.random() * 5);

  Alert.alert(
    'Mensagem do dia!',
    messages[randomNum]
  );
  //alert('Mensagem do dia!', messages[randomNum]);
};

AppRegistry.registerComponent('Frases', () => App);
